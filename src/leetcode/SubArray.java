package leetcode;

public class SubArray {


    public static void main(String[] args) {
        subset(new int[]{5, 2, 3, 4, 9 , 8});
    }

    private static void subset(int[] set) {
        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < set.length -1; x++) {
            for (int y = x; y < set.length; y++) {
                sb.append('[');
                for (int z = x; z <= y; z++) {

                    sb.append(set[z]);
                }
                sb.append(']');
            }
        }
        System.out.println(sb.toString());
    }
}
