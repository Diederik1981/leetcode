package leetcode.monthly;

public class LongHarmSubSTring {


    public static void main(String[] args) {
        System.out.println(numberOfSteps(14));
    }

    public static int numberOfSteps (int num) {

        int count = 0;
        int lastpost = 0;
        for ( int x = 0; x < 32; x++) {
            if ((num >> x & 1) == 1) {
                lastpost = x;
                count ++;
            }
        }
        if (lastpost == 0) {
            return 0;
        }

        return count + lastpost;
    }

}
