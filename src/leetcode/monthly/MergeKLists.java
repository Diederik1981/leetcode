package leetcode.monthly;

import leetcode.ListNode;

import java.util.TreeSet;

public class MergeKLists {

    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        root.next = new ListNode(4);

        ListNode r2 = new ListNode(2);
        r2.next = new ListNode(4);

        System.out.println(mergeKLists(new ListNode[]{root, r2}));
    }

    public static ListNode mergeKLists(ListNode[] lists) {

        ListNode root = new ListNode();
        ListNode resp = root;

        TreeSet<Integer> values = new TreeSet<>();
        for (int x = 0; x < lists.length; x++) {
            ListNode tmp = lists[x];
            while (tmp != null) {
                values.add(tmp.val);
                tmp = tmp.next;
            }
        }


        for (Integer x : values) {
            for (int y = 0; y < lists.length; y++) {
                while (lists[y] != null && lists[y].val == x) {
                    root.next = lists[y];
                    root = root.next;
                    lists[y] = lists[y].next;
                }
            }
        }


        return resp.next;

    }
}

