package leetcode.monthly;

import java.util.LinkedList;
import java.util.Queue;

public class PathWithMinimumEffort {

    public static void main(String[] args) {
        System.out.println(minimumEffortPath(new int[][]{
                {1, 2, 2},
                {3, 8, 2},
                {5, 3, 5}}));
    }


    public static int minimumEffortPath(int[][] heights) {
        boolean[][] dists = new boolean[heights.length][heights[0].length];

        dists[0][0] = true;


        Queue<Pos> q = new LinkedList();
        q.add(Pos.of(0, 0));

        while (!q.isEmpty()) {
            Pos tmp = q.poll();
            if (tmp.col > 0) {
                int tmpDist = tmp.distLeft(heights);
                System.out.println(tmpDist);
            }
            if (tmp.col > heights[0].length) {
                int tmpDist = tmp.distRight(heights);
                System.out.println(tmpDist);
            }
        }


        return 0;
    }

    static class Pos {
        public final int row;
        public final int col;

        Pos(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public static Pos of(int row, int col) {
            return new Pos(row, col);
        }

        private int dist(int[][] arr, int row, int col) {
            return Math.abs(arr[row][col] - arr[this.row][this.col]);
        }

        public int distLeft(int[][] arr) {
            return dist(arr, row, col - 1);
        }

        public int distRight(int[][] arr) {
            return dist(arr, row, col + 1);
        }

        public int distUp(int[][] arr) {
            return dist(arr, row - 1, col);
        }

        public int distDown(int[][] arr) {
            return dist(arr, row + 1, col);
        }

    }
}


