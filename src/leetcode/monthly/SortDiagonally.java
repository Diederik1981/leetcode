package leetcode.monthly;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SortDiagonally {


    public static void main(String[] args) {
        int[][] ints = diagonalSort(new int[][]{{3, 3, 1, 1}, {2, 2, 1, 2}, {1, 1, 1, 2}});

        for (int[] a: ints) {
            System.out.println();
            for (int i : a) {
                System.out.print(i + ",");
            }
        }

    }

    public static int[][] diagonalSort(int[][] mat) {

        int rows = mat.length;
        int cols = mat[0].length;

        for (int x = 0; x < rows; x++) {

            int row = x;
            int col = 0;

            List<Integer> tmp = new ArrayList<>();
            while (row < rows && col < cols) {
                tmp.add(mat[row][col]);
                row++;
                col++;
            }

            row = x;
            col = 0;
            Collections.sort(tmp);
            for (int i : tmp) {
                mat[row][col]  = i;
                row++;
                col++;
            }
        }
        for (int x = 1; x < cols; x++) {

            int row = 0;
            int col = x;

            List<Integer> tmp = new ArrayList<>();
            while (row < rows && col < cols) {
                tmp.add(mat[row][col]);
                row++;
                col++;
            }

            row = 0;
            col = x;
            Collections.sort(tmp);
            for (int i : tmp) {
                mat[row][col]  = i;
                row++;
                col++;
            }
        }

        return mat;
    }
}
