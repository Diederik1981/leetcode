package leetcode.monthly;

public class ArithmeticSlices {

    public static void main(String[] args) {
        System.out.println(numberOfArithmeticSlices(new int[]{1, 2, 3, 4,5}));
    }


    public static int numberOfArithmeticSlices(int[] A) {

        if (A.length < 3) {
            return 0;
        }
        int total = 0;
        int count = 2;
        int diff = A[1] - A[0];
        for (int x = 2; x < A.length; x++) {
            int tmpDiff = A[x] - A[x -1];
            if (tmpDiff == diff) {
                if (++count >= 3) {
                    total += count -2;
                }
            } else {
                count = 2;
                diff = tmpDiff;
            }
        }
        return total;
    }
}
