package leetcode.hard;

public class FirstMissingPositive {

    public static void main(String[] args) {

        System.out.println(firstMissingPositive(new int[]{3, 4, -1, 1}));

    }

    public static int firstMissingPositive(int[] nums) {
        boolean[] pos = new boolean[302];
        for (int i : nums) {
            if (i > 0 && i < 302) {
                pos[i] = true;
            }
        }
        int index = 1;
        while (true) {
            if (!pos[index]) {
                return index;
            }
            index++;
        }
    }
}