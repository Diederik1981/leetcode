package leetcode.hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *  Not the O( log( m + n )) asked.
 *
 */
public class Median2SortedArray {

	public static double findMedianSortedArrays(int[] nums1, int[] nums2) {

		System.out.println(Arrays.asList(nums1, nums2));
		List<Integer> lst = new ArrayList<>();
		for (int i = 0; i < nums1.length; i++) {
			lst.add(nums1[i]);
		}
		for (int i = 0; i < nums2.length; i++) {
			lst.add(nums2[i]);
		}
		Collections.sort(lst);
		if (lst.size() % 2 == 0) {
			return ((double)(lst.get(lst.size() / 2) + lst.get(lst.size() / 2 + 1))) /2;
		}

		return lst.get(lst.size()/2);
	}

	public static void main(String[] args) {
		System.out.println(findMedianSortedArrays(new int[] { 1, 5, 6, 7 }, new int[] { 1, 2, 3 }));
	}
}
