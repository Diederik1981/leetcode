package leetcode;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Helper {

	public static int[] toIntArray(String arr) {
		String str = arr.substring(1, arr.length() - 1);
		String[] split = str.split(",");
		return Arrays.stream(split).mapToInt(Integer::parseInt).toArray();
	}
	
	
	public static void print(int[]  arr) {		
		if (arr == null) {
			System.out.println("'null pointer'");
		} else {
			System.out.println(Arrays.stream(arr).mapToObj(o -> String.valueOf(o)).collect(Collectors.joining(", ", "[", "]")));			
		}
	}
	
	public static void print(String[]  arr) {		
		if (arr == null) {
			System.out.println("'null pointer'");
		} else {
			System.out.println(Arrays.stream(arr).collect(Collectors.joining(", ", "[", "]")));			
		}
	}
}
