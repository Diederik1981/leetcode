package leetcode;

import java.util.List;
import java.util.stream.Collectors;

public class Node {
	public int val;
	public List<Node> children;

	public Node() {
	}

	public Node(int _val) {
		val = _val;
	}

	public Node(int _val, List<Node> _children) {
		val = _val;
		children = _children;
	}

	@Override
	public String toString() {
		return val + ", " + (children == null ? "null" : children.stream().map(Node::toString).collect(Collectors.joining(", ", "[", "]")));
	}
};