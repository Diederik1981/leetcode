package leetcode;

public class Primes {

	private static boolean[] primes = null;
	static {

		primes = new boolean[1000001];
		for (int x = 3; x < primes.length; x += 2) {
			primes[x] = true;
		}
		primes[2] = true;
		
		
		for (int x = 3; x <= 500000; x++) {
			if (primes[x] == true) {
				int inc = x + x;
				for (int y = x + inc; y < primes.length; y += inc) {
					primes[y] = false;
				}
			}
		}

	}
}
