package leetcode;

public class Main {
    public static void main(String[] args) {


    }


    public static int findMaxConsecutiveOnes(int[] nums) {
        int max = 0;
        int count = 0;

        for (int x = 0; x < nums.length; x++) {
            if (nums[x] == 1) {
                count++;
                max = Math.max(max, count);
            } else {
                count = 0;
            }
        }

        return max;

    }

}


