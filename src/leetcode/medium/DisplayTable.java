package leetcode.medium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

public class DisplayTable {

	public List<List<String>> displayTable(List<List<String>> orders) {

		Map<String, Map<String, Integer>> tables = new HashMap<>();
		Set<String> dishes = new TreeSet<>();
		for (List<String> list : orders) {
			if (tables.get(list.get(1)) == null) {
				tables.put(list.get(1), new HashMap<>());
			}
			dishes.add(list.get(2));
		}

		for (List<String> list : orders) {
			Map<String, Integer> map = tables.get(list.get(1));
			if (map.get(list.get(2)) == null) {
				map.put(list.get(2), 1);
			} else {
				map.put(list.get(2), map.get(list.get(2)) + 1);
			}
		}
		List<List<String>> response = new ArrayList<List<String>>();
		List<String> base = new ArrayList<>();
		base.add("Table");
		base.addAll(dishes);

		for (Entry<String, Map<String, Integer>> entry : tables.entrySet()) {
			List<String> list = new ArrayList<>();
			list.add(entry.getKey());
			for (String dish : dishes) {
				list.add(String.valueOf(Optional.ofNullable(entry.getValue().get(dish)).orElse(0)));
			}
			response.add(list);
		}

		Collections.sort(response, new Comparator<List<String>>() {
			public int compare(List<String> p1, List<String> p2) {
				return Integer.valueOf(p1.get(0)) - Integer.valueOf(p2.get(0));
			}
		});

		response.add(0, base);
		return response;
	}
}
