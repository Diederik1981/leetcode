package leetcode.medium;

public class CountNumberOfTeams {

	public static void main(String[] args) {
		// print();
		System.out.println(numTeams(new int[] { 2, 5, 3, 4, 1 }));

	}

	public static int numTeams(int[] rating) {
		int s = rating.length;
		if (s < 3) {
			return 0;
		}
		int counter = 0;
		for (int x = 0; x < s - 2; x++) {
			for (int y = x + 1; y < s - 1; y++) {
				for (int z = y + 1; z < s; z++) {
					if ((rating[x] > rating[y] && rating[y] > rating[z]) || (rating[x] < rating[y] && rating[y] < rating[z])) {
						counter++;
					}
				}
			}
		}

		return counter;
	}
}
