package leetcode.medium;

class SubrectangleQueries {
	private final int[][] rect;
	private final int maxRow;
	private final int maxCol;

	public SubrectangleQueries(int[][] rectangle) {
		rect = rectangle;
		maxCol = rect.length;
		maxRow = rect[0].length;
	}

	public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
		for (int row = row1; row <= row2; row ++) {
			for (int col = row1; col <= row2; col ++) {
				rect[row][col]  = newValue;
			}
			
		}
	}

	public int getValue(int row, int col) {
		return rect[row][col];
	}
}