package leetcode.medium;

public class Divide2Integers {

	public static int divide(int dividend, int divisor) {
		int id = 1;
		if (divisor < 0) {
			id = -1;
			divisor *= id;
		}
		if (dividend < 0) {
			id *= -1;
			dividend *= -1;
		}
		
		int total = 0;
		while (dividend >= divisor) {
			int tmp = divisor;
			int mag = 1;
			while (tmp <= dividend) {
				mag = mag << 1;
				tmp = tmp << 1;
			}
			tmp = tmp >> 1;
			dividend -= tmp;
			mag = mag >> 1;
			total += mag;
			System.out.println("total" + total);
			System.out.println("divid" + dividend);
		}
		return total * id;
	}

	public static void main(String[] args) {
		System.out.println(divide(10000000, 3));
	}
}
