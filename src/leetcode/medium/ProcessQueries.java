package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

public class ProcessQueries {

	public static void main(String[] args) {
		int[] processQueries = processQueries(new int[] { 3, 1, 2, 1 }, 5);

		for (int i = 0; i < processQueries.length; i++) {
			System.out.println(processQueries[i]);

		}
	}

	public static int[] processQueries(int[] queries, int m) {
		boolean[] hits = new boolean[10001];
		int shift = 0;

		List<Integer> tmp = new ArrayList<Integer>();

		for (int x = 0; x < queries.length; x++) {
			if (!hits[queries[x] - 1]) {
				queries[x] = queries[x] - 1 + shift;
				hits[queries[x] - 1] = true;
				
				shift++;
				tmp.add(queries[x]);
				System.out.println("shift" + shift);
			} else {
				for (int i = tmp.size() - 1; i >= 0; i--) {
					if (tmp.get(i).equals(queries[x])) {

						Integer remove = tmp.remove(i);
						tmp.add(remove);
						queries[x] = tmp.size() - i - 1;
					}
				}
			}
		}

		return queries;
	}
//		public static int[] processQueries(int[] queries, int m) {
//
//		List<Integer> collect = IntStream.rangeClosed(1, m).boxed().collect(Collectors.toList());
//
//		
//		
//		for (int j = 0; j < queries.length; j++) {
//			int tmp = queries[j];
//			int indexOf = collect.indexOf(queries[j]);
//			queries[j] = indexOf;
//			collect.remove(indexOf);
//			collect.add(0, tmp);
//		}
//
//		return queries;
//	}
}
