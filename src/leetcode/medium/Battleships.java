package leetcode.medium;

public class Battleships {

	public static void main(String[] args) {
		System.out.println(countBattleships(new char[][] { { 'X', '.', '.', 'X' }, { '.', '.', '.', 'X' }, { '.', '.', '.', 'X' } }));
	}

	public static int countBattleships(char[][] board) {
		int count = 0;
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board[0].length; y++) {
				if (board[x][y] == 'X' && !(hasLeftNeighbor(board, x, y) || hasTopNeighbor(board, x, y))) {
					count++;
					y++;
				}
			}
		}
		return count;
	}

	private static boolean hasLeftNeighbor(char[][] board, int x, int y) {
		if (x == 0)
			return false;
		if (board[x - 1][y] == 'X')
			return true;
		return false;
	}

	private static boolean hasTopNeighbor(char[][] board, int x, int y) {
		if (y == 0)
			return false;
		if (board[x][y - 1] == 'X')
			return true;
		return false;
	}

}
