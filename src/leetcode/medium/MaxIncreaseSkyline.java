package leetcode.medium;

public class MaxIncreaseSkyline {

	public static void main(String[] args) {
		System.out.println(maxIncreaseKeepingSkyline(new int[][] { { 3 } }));
		
		
//		, 0, 8, 4 }, // 8
//				{ 2, 4, 5, 7 }, // 7
//				{ 9, 2, 6, 3 }, // 9
//				{ 0, 3, 1, 0 } }));// 3
//            9  4  8  7 
	}

	public static int maxIncreaseKeepingSkyline(int[][] grid) {
		int len = grid.length;
		int[] cols = new int[len];
		int[] rows = new int[len];

		for (int x = 0; x < len; x++) {

			int maxCol = 0;
			int maxRow = 0;
			for (int y = 0; y < len; y++) {
				if (grid[y][x] > maxCol) {
					maxCol = grid[y][x];
				}
				if (grid[x][y] > maxRow) {
					maxRow = grid[x][y];
				}
			}
			cols[x] = maxCol;
			rows[x] = maxRow;

		}
		int count = 0;
		for (int x = 0; x < len; x++) {
			for (int y = 0; y < len; y++) {
				count += Math.min(cols[x], rows[y]) - grid[x][y];
			}
		}

		return count;
	}
}
