package leetcode.medium;

import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {

	public static void main(String[] args) {
		System.out.println((int)'.');
		System.out.println((int)'0');
		System.out.println((int)'9');
//		System.out.println(isValidSudoku(new char[][]
//				{{'5','3','.', '.','7','.', '.','.','.'},
//				 {'6','.','.', '1','9','5', '.','.','.'},
//				 {'.','9','8', '.','.','.', '.','6','.'},
//				
//				{'8','.','.', '.','6','.', '.','.','3'},
//				{'4','.','.', '8','.','3', '.','.','1'},
//				{'7','.','.', '.','2','.', '.','.','6'},
//				
//				{'.','6','.', '.','.','.' ,'2','8','.'},
//				{'.','.','.', '4','1','9', '.','.','5'},
//				{'.','.','.', '.','8','.', '.','7','9'}
//				}));
		
//		System.out.println(isValidSudoku(new char[][]
//		{{'.','.','5','.','.','.','.','.','.'},
//		 {'.','.','.','8','.','.','.','3','.'},
//		 {'.','5','.','.','2','.','.','.','.'},
//		 {'.','.','.','.','.','.','.','.','.'},
//		 {'.','.','.','.','.','.','.','.','9'},
//		 {'.','.','.','.','.','.','4','.','.'},
//		 {'.','.','.','.','.','.','.','.','7'},
//		 {'.','1','.','.','.','.','.','.','.'},
//		 {'2','4','.','.','.','.','9','.','.'}}));
		
		System.out.println(isValidSudoku(new char[][]

		{{'.','.','4',  '.','.','.',  '6','3','.'},
		 {'.','.','.',  '.','.','.',  '.','.','.'},
		 {'5','.','.',  '.','.','.',  '.','9','.'},
		 
		 {'.','.','.',  '5','6','.',  '.','.','.'},
		 {'4','.','3',  '.','.','.',  '.','.','1'},
		 {'.','.','.',  '7','.','.',  '.','.','.'},
		 
		 {'.','.','.',  '5','.','.',  '.','.','.'},
		 {'.','.','.',  '.','.','.',  '.','.','.'},
		 {'.','.','.',  '.','.','.',  '.','.','.'}}));

	}
	
	
	public static boolean isValidSudoku(char[][] board) {
		int a = 0;
		int b = 0;
		int tmp = 0;
		for (int x = 0; x < 9; x++) {
			a= 0;
			b= 0;
			for (int y = 0; y < 9; y++) {
				//char horC = board[x][y];
				tmp = (int) board[x][y] - 48;
				if (tmp != -2) {
					if ((a & (1L << tmp)) != 0) {
						return false;
					}
					a |= 1 << tmp;
				}
				tmp = (int) board[y][x] - 48;
				if (tmp != -2) {
					if ((b & (1L << tmp)) != 0) {
						return false;
					}
					b |= 1 << tmp;
				}
			}
		}

		for (int x = 0; x < 9; x += 3) {
			for (int y = 0; y < 9; y += 3) {
				Set<Character> bl = new HashSet<>();
				for (int z = 0; z < 3; z += 1) {
					for (int zz = 0; zz < 3; zz += 1) {
						char blC = board[z + x][zz + y];
						if (blC != '.') {
							if (bl.contains(blC)) {
								return false;
							}
							bl.add(blC);
						}
					}
				}
			}
		}

		return true;
	}
		
	
	public static boolean isValidSudoku2(char[][] board) {

		int counter = 0;
		int segment = 0;
		
		int[] arr = new int[27];
		System.out.println(board.length);
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				int tmp = (int) board[x][y] - 48;
				if (tmp != -2) {
					if ((arr[segment] & (1L << tmp)) != 0) {
						return false;
					}
					arr[segment] |= 1 << tmp;
				}
				counter++;
				if (counter == 3) {
					segment++;
					counter = 0;
				}
			}
		}

		for (int x = 0; x < 27; x += 3) {
			if ((arr[x] & arr[x + 1]) != 0 || (arr[x] & arr[x + 2]) != 0 || (arr[x + 1] & arr[x + 2]) != 0) {
				return false;
			}
		}

		for (int x = 0; x < 19; x += 9) {
			for (int y = 0; y < 3; y++) {
				int a = arr[x + y];
				int b = arr[x + y + 3];
				int c = arr[x + y + 6];
				if ((a & b) != 0 || (a & c) != 0 || (b & c) != 0) {
					return false;
				}
			}
		}
		
		for (int x = 0; x < 9; x++) {
			Set<Character> chars = new HashSet<>();
			System.out.println();
			for (int y = 0; y < 9; y++) {
				System.out.print(board[y][x] + ",");
				char c = board[y][x];
				if (c != '.') {

					if (chars.contains(c)) {
						return false;
					}
					chars.add(board[y][x]);
				}
			}
		}

		return true;
	}
	
	 public static boolean isValidSudoku3(char[][] board) {
			for (int x = 0; x < 9; x++) {
				Set<Character> hor = new HashSet<>();
				Set<Character> ver = new HashSet<>();
				for (int y = 0; y < 9; y++) {
					char horC = board[x][y];
					if (horC != '.') {
						if (hor.contains(horC)) {
							return false;
						}
						hor.add(horC);
					}
					char verC = board[y][x];
					if (verC != '.') {
						if (ver.contains(verC)) {
							return false;
						}
						ver.add(verC);
					}
				}
			}

			for (int x = 0; x < 9; x += 3) {
				for (int y = 0; y < 9; y += 3) {
					Set<Character> bl = new HashSet<>();
					for (int z = 0; z < 3; z += 1) {
						for (int zz = 0; zz < 3; zz += 1) {
							char blC = board[z + x][zz + y];
							if (blC != '.') {
								if (bl.contains(blC)) {
									return false;
								}
								bl.add(blC);
							}
						}
					}
				}
			}

			return true;
		}
}
