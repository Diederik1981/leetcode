package leetcode.medium;

public class TrappingRainwater {

    public static void main(String[] args) {
        System.out.println(trap(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
        System.out.println(trap(new int[]{0, 1, 2, 0,100,25 , 50, 0}));
        System.out.println(trap(new int[]{}));
    }

    public static int trap(int[] height) {
        if (height.length < 2) {
            return 0;
        }
        int left = 0;
        int right = height.length - 1;

        int maxLeft = height[left];
        int maxRight = height[right];

        int count = 0;

        while (left <= right) {

            if (maxLeft < maxRight) {
                int tmp = height[left];
                if (tmp < maxLeft) {
                    count += maxLeft - tmp;
                } else {
                    maxLeft = tmp;
                }
                left++;
            } else {
                int tmp = height[right];
                if (tmp < maxRight) {
                    count += maxRight - tmp;
                } else {
                    maxRight = tmp;
                }
                right--;

            }
        }

        return count;
    }

}
