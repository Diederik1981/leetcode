package leetcode.medium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DiagonalSort {

	public static void main(String[] args) {
		diagonalSort(new int[][] { { 3, 3, 1, 1 }, { 2, 2, 1, 2 }, { 1, 1, 1, 2 } });
	}

	public static int[][] diagonalSort(int[][] mat) {
		List<Integer> list = new ArrayList<>();
		for (int x = 0; x < mat.length; x++) {
			for (int y = 0; y < mat.length; y++) {
				list.add(mat[x][y]);
			}
		}
		Collections.sort(list, Comparator.reverseOrder());
		System.out.println(list);

		int row = 0;
		int col = 0;
		int index = list.size() - 1;
		while (!list.isEmpty()) {
			for (int x = col; x < mat[0].length; x++) {				
				mat[row][x] = list.remove(index--);
			}
			col++;
			row++;
			for (int x = row; x < mat.length; x++) {
				mat[x][col] = list.remove(index--);				
			}
			System.out.println(list);
		}

		for (int x = 0; x < mat[0].length; x++) {
			System.out.println(mat[0][x]);
			System.out.println(mat[1][x]);
		}

		return null;
	}
}
