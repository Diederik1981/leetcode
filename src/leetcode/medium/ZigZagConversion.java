package leetcode.medium;

public class ZigZagConversion {
	public static void main(String[] args) {
		
		
		System.out.println(convert("PAYPALIASSAAQWR", 4));
		System.out.println(String.valueOf(new char[] {'a',0,0, 'c'}));
	}

	public static String convert(String s, int numRows) {

		if (numRows == 1) {
			return s;
		}
		if (numRows == 0) {
			return "";
		}

		int patternSize = (numRows - 1) * 2;
		int halfSize = (numRows - 1);

		StringBuilder[] sb = new StringBuilder[numRows];
		for (int x = 0; x < numRows; x++) {
			sb[x] = new StringBuilder();
		}
		int patPos = 0;
		int sbPos = 0;
		for (int x = 0; x < s.length(); x++) {
			//System.out.println(sbPos);
			sb[sbPos].toString();
			sb[sbPos].append(s.charAt(x));

			if (patPos < halfSize) {
				sbPos++;
			} else {
				sbPos--;
			}

			patPos++;
			patPos %= (patternSize);
		}

		for (int x = 1; x < sb.length; x++) {
			sb[0].append("-");
			sb[0].append(sb[x]);
			
		}
		//System.out.println(s.length());
		return sb[0].toString();
	}
}