package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

public class SubSet {


    public static void main(String[] args) {
        System.out.println(subsets(new int[]{1,2,3}));
    }

    public static List<List<Integer>> subsets(int[] nums) {

        int options = 0;
        int count = 0;
        for (int x = 0; x < nums.length; x++) {
            options = options << 1 | 1;
            count++;
        }
        List<List<Integer>> lists = new ArrayList<>();
        for (int x = 0; x <= options; x++) {
            List<Integer> list = new ArrayList<>();
            int z = x;
            for (int y = 0; y < count; y++) {
                if ((x >> y & 1) == 0) {
                    list.add(nums[y]);
                }
            }
            lists.add(list);
        }
        return lists;
    }




}
