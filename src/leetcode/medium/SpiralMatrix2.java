package leetcode.medium;

public class SpiralMatrix2 {

    public static void main(String[] args) {
        int[][] m = generateMatrix(9);

        for (int[] a : m) {
            System.out.println();
            for (int b : a) {
                System.out.printf("%02d,", b);
            }
        }
    }

        public static int[][] generateMatrix2(int n) {
        int[][] matrix = new int[n][n];
        int[][] dirs = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        int dir = 0, row = 0, col = -1;
        for (int x = 0; x < n * n; x++) {
            matrix[row += dirs[dir][0]][col += dirs[dir][1]] = x + 1;
            if (row + dirs[dir][0] == n || row + dirs[dir][0] < 0 || col + dirs[dir][1] == n || col + dirs[dir][1] < 0 || matrix[row + dirs[dir][0]][col + dirs[dir][1]] != 0) dir = (dir + 1) % 4;
        }
        return matrix;
    }


    public static int[][] generateMatrix(int n) {
        int[][] arr = new int[n][n];

        int size = n * n, hor = n, ver = n - 1, count = 0, hdir = 1, vdir = 0, currentRow = 0, currentCol = -1, tmpDistance = n;

        for (int x = 0; x < size; x++) {
            arr[currentRow += vdir][currentCol += hdir] = ++count;

            if (--tmpDistance == 0) {
                if (hdir != 0) {
                    hor--;
                    vdir = hdir;
                    hdir = 0;
                    tmpDistance = ver;
                } else {
                    ver--;
                    hdir = -vdir;
                    vdir = 0;
                    tmpDistance = hor;
                }
            }
        }
        return arr;
    }


//    public static int[][] generateMatrix(int n) {
//
//        int[][] resp = new int[n][n];
//
//
//        int row = 0;
//        int col = -1;
//
//        int right = n;
//        int down = n - 1;
//        int left = right - 1;
//        int up = down - 1;
//
//        int size = n * n;
//        int count = 0;
//
//        while (true) {
//
//            if (right <= 0) {
//                break;
//            }
//            for (int x = 0; x < right; x++) {
//                col++;
//                resp[row][col] = ++count;
//            }
//            right--;
//            right--;
//
//            if (down <= 0) {
//                break;
//            }
//            for (int x = 0; x < down; x++) {
//                row++;
//                resp[row][col] = ++count;
//            }
//            down--;
//            down--;
//
//            if (left <= 0) {
//                break;
//            }
//            for (int x = 0; x < left; x++) {
//                col--;
//                resp[row][col] = ++count;
//            }
//            left--;
//            left--;
//
//            if (up <= 0) {
//                break;
//            }
//            for (int x = 0; x < up; x++) {
//                row--;
//                resp[row][col] = ++count;
//            }
//            up--;
//            up--;
//        }
//
//        return resp;
//
//    }

}
