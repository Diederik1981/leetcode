package leetcode.medium;

import java.util.NavigableSet;
import java.util.TreeSet;

public class ContainsNearbyAlmostDuplicate {

	public static void main(String[] args) {
		System.out.println(containsNearbyAlmostDuplicate(new int[] { 1, 5, 9, 1, 5, 9 }, 2, 1));
		System.out.println(containsNearbyAlmostDuplicate(new int[] { 1, 2, 3, 1 }, 3, 0));
		System.out.println(containsNearbyAlmostDuplicate(new int[] { 1, 2 }, 0, 1));
		System.out.println(containsNearbyAlmostDuplicate(new int[] { 7, 1, 3 }, 2, 3));
		System.out.println(containsNearbyAlmostDuplicate(new int[] { 1, 1, 3 }, 2, 3));
	}

	/**
	 * @param k max index differece
	 * @param t max value differenc3e
	 */
	public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
		if (nums.length < 2) {
			return false;
		}
		if (k == 0) {
			return false;
		}
		if (k > nums.length - 1) {
			k = nums.length - 1;
		}
		NavigableSet<Long> set = new TreeSet<>();
		for (int x = 0; x < k; x++) {
			set.add((long) nums[x]);
		}
		if (set.size() != k) {
			return true;
		}
		for (int x = 0; x < nums.length - 1; x++) {
			set.remove((long) nums[x]);
			if (x + k < nums.length) {
				set.add((long) nums[x + k]);
			}

			if (checkNear(set, nums[x], t)) {
				return true;
			}
		}
		return false;

	}

	private static boolean checkNear(NavigableSet<Long> set, long key, long dist) {
		Long ceiling = set.ceiling(key);
		if (ceiling != null && Math.abs(key - ceiling) <= dist) {
			return true;
		}
		Long floor = set.floor(key);
		if (floor != null && Math.abs(key - floor) <= dist) {
			return true;
		}
		return false;
	}

	private static void addKey(NavigableSet<Pair> set, long key) {
		Pair tmp = new Pair(key);
		Pair ceiling = set.ceiling(tmp);
		if (ceiling != null && key == ceiling.val) {
			ceiling.count++;
		} else {
			set.add(tmp);
		}
	}

	private static void removeKey(NavigableSet<Pair> set, long key) {
		Pair tmp = new Pair(key);
		Pair ceiling = set.ceiling(tmp);
		ceiling.count--;
		if (ceiling.count == 0) {
			set.remove(ceiling);
		}
	}

	static class Pair implements Comparable<Pair> {
		public Long val;
		public int count;

		public Pair(long val) {
			this.val = val;
			count = 1;
		}

		@Override
		public boolean equals(Object obj) {
			return val.equals(obj);
		}

		@Override
		public String toString() {
			return String.valueOf(val) + "(" + count + ")";
		}

		@Override
		public int compareTo(Pair o) {
			return val.compareTo(o.val);
		}
	}
}
