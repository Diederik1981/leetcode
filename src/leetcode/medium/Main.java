package leetcode.medium;

import java.util.NavigableSet;
import java.util.TreeSet;

public class Main {
	
	public static void main(String[] args) {
		NavigableSet<Integer> s = new TreeSet<>();
		s.add(1);
		s.add(100);
		s.add(50);
		s.add(25);
		
		System.out.println(s);
		System.out.println("ceil:" + s.ceiling(30));
		System.out.println("floor:" + s.floor(30));
		System.out.println("close:" + s.ceiling(50));
		
		
	}

//	public Object getBestMatch(SortedSet set, Object item)
//	{
//		if (set.contains(item))
//			return item;
//		Set<Integer> tail = set.tailSet();
//		if (tail.isEmpty())
//			return null;
//		return tail.first();
//	}
	
}
