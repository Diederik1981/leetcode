package leetcode.medium;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MinCharsToUnique {


    /**
     * stolen and improved by treeset.
     */
    public int minDeletions(String s) {
        int[] cnt = new int[26];
        for (char c : s.toCharArray()) {
            ++cnt[c - 'a'];
        }

        Set<Integer> set = new TreeSet<>();
        int res = 0;
        for (int c : cnt) {
            while (c > 0 && !set.add(c)) {
                --c;
                ++res;
            }
        }
        return res;
    }


// my solutions

//    public static int minDeletions(String s) {
//        int[] charcount = new int['z' + 1 - 'a'];
//        for (int x = 0; x < s.length(); x++) {
//            charcount[s.charAt(x) - 'a']++;
//        }
//        Set<Integer> treeset = new TreeSet<>();
//        int totalCount = 0;
//        for (int x = 0; x < charcount.length; x ++) {
//            int tmp = charcount[x];
//            if(tmp ==0) {
//                continue;
//            }
//            while (treeset.contains(tmp) && tmp != 0) {
//                totalCount++;
//                tmp--;
//            }
//            treeset.add(tmp);
//        }
//        return totalCount;
//    }



//    public static int minDeletions(String s) {
//        int[] charcount = new int['z' + 1 - 'a' + 1];
//        for (int x = 0; x < s.length(); x++) {
//            charcount[s.charAt(x) - 'a' + 1]++;
//        }
//        Set<Integer> hits = new HashSet<>();
//
//        int totalCount = 0;
//        for (int x = charcount.length - 1; x >= 0; x--) {
//            int tmp = charcount[x];
//            if (tmp == 0) {
//                continue;
//            }
//            while (tmp != 0 && hits.contains(tmp)) {
//                totalCount++;
//                tmp--;
//            }
//            hits.add(tmp);
//        }
//        return totalCount;
//    }

//    public static void main(String[] args) {

//        System.out.println( minDeletions("ceabaacb"));
//        System.out.println( minDeletions("aaabbbcc"));
//        System.out.println( minDeletions("bbcebab"));
//        System.out.println( minDeletions("ceabaacb"));
//        System.out.println( minDeletions("ebbccaaa"));
//        System.out.println( minDeletions("abcdefghijklmnopqrstuvwxwz"));
    }
//}
