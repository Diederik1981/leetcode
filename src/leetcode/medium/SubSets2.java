package leetcode.medium;

import java.util.List;

public class SubSets2 {

	public static void main(String[] args) {

		subsets(new int[] {1,1,1,1,2});
	}

    public static List<List<Integer>> subsets(int[] nums) {
        
		int nr = 0;
		for (int x = 0; x < nums.length; x++) {
			nr <<= 1;
			nr++;
		}
		
		for (int x = 0; x <= nr; x++) {			
			System.out.println(Integer.toBinaryString(x));
		}
		
		System.out.println(Integer.toBinaryString(nr));
		
		return null;
	}
}
