package leetcode.medium;

public class LongestPalindromicSubstring {

	public static String longestPalindrome(String s) {
		if (s == null || s.isEmpty()) {
			return "";
		}
		if (s.length() == 1) {
			return s;
		}
		if (s.length() == 2) {
			if (s.charAt(0) == s.charAt(1)) {
				return s;
			} else {
				return String.valueOf(s.charAt(0));
			}
		}
		String longest = "";
		for (int x = 0; x < s.length() - 1; x++) {
			String tmp = getPalinFor(s, x, x);
			if (tmp.length() > longest.length()) {
				longest = tmp;
			}
			if (s.charAt(x) == s.charAt(x + 1)) {
				tmp = getPalinFor(s, x, x + 1);
				if (tmp.length() > longest.length()) {
					longest = tmp;
				}
			}
		}
		return longest;
	}

	static String getPalinFor(String s, int x1, int x2) {

		int distance = 1;
		int count = 0;
		while (x1 - distance >= 0 && x2 + distance < s.length()) {
			if (s.charAt(x1 - distance) == s.charAt(x2 + distance)) {
				count++;
			} else {
				break;
			}
			distance++;
		}

		return s.substring(x1 - count, x2 + count + 1);
	}

	public static void main(String[] args) {
//		System.out.println(longestPalindrome("a"));
//		System.out.println(longestPalindrome("aa"));
//		System.out.println(longestPalindrome("ab"));
//		System.out.println(longestPalindrome("aba"));
//		System.out.println(longestPalindrome("abba"));
//		System.out.println(longestPalindrome("csdfasdabvbvbasfsdfsdfsddddddddddddddddddddddddddddddddd"));
		System.out.println(longestPalindrome("ccc"));
	}
}
