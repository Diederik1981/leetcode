package leetcode.medium;

public class SumRootToLeafNumbers {


    public static void main(String[] args) {


        TreeNode root = new TreeNode(4);
        TreeNode nine = new TreeNode(9);
        TreeNode zero = new TreeNode(0);
        TreeNode five = new TreeNode(5);
        TreeNode one = new TreeNode(1);

        root.right = zero;
        root.left = nine;
        nine.left = five;
        nine.right = one;
        System.out.println(sumNumbers(root));

    }

    private static int sum = 0;

    public static int sumNumbers(TreeNode root) {
        System.out.println(root);
        sum(root, 0);
        return sum;
    }

    private static void sum(TreeNode root, int tmp) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            sum += tmp + root.val;
            System.out.println(tmp + root.val);
            return;
        }
        tmp += root.val;
        tmp *= 10;

        sum(root.left, tmp);
        sum(root.right, tmp);

    }


    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    ", left=" + left +
                    ", right=" + right +
                    '}';
        }
    }
}
