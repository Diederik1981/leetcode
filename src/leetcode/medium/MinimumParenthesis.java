package leetcode.medium;

public class MinimumParenthesis {

	public static void main(String[] args) {
		System.out.println(minAddToMakeValid("))())(())"));
	}

	public static int minAddToMakeValid(String S) {
		int left = 0;
		int right = 0;
		for (char c : S.toCharArray()) {
			if (c == ')') {
				if (right > 0) {
					right--;
				} else {
					left++;
				}
			} else {
				right++;
			}

		}

		return left + right;
	}
}
