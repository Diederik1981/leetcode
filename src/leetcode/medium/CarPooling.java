package leetcode.medium;

public class CarPooling {


    public static void main(String[] args) {
//        System.out.println(carPooling(new int[][]{{3, 2, 7}, {3, 7, 9}, {8, 3, 9}}, 11));
        System.out.println(carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 4));
    }


    public static boolean carPooling(int[][] trips, int capacity) {


        int[] stations = new int[1001];

        for (int x = 0; x < trips.length; x++) {
            stations[trips[x][1]] += trips[x][0];
            stations[trips[x][2]] -= trips[x][0];
        }

        int passengers = 0;
        for (int station : stations) {
            passengers += station;
            if (passengers < 0 ){
                passengers = 0;
            }
            if (passengers > capacity) {
                return false;
            }
        }
        return true;
    }
}
