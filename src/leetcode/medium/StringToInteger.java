package leetcode.medium;

public class StringToInteger {

	public static void main(String[] args) {
		// System.out.println(myAtoi("42"));
		// System.out.println(myAtoi(" -42"));
		//System.out.println(myAtoi(" -42"));
		//System.out.println(myAtoi(" +42"));
		System.out.println(myAtoi("-2147483648"));
		System.out.println(myAtoi("2147483800"));
		System.out.println(Integer.MIN_VALUE);
		// System.out.println(myAtoi(" 42 asdf"));
		// System.out.println(myAtoi("sdf 42"));
	}

	/**
	 * failed :(
	 * @param str
	 * @return
	 */
	public static int myAtoi(String str) {
		int index = 0;
		int id = 1;
		for (int x = 0; x < str.length(); x++) {
			if (str.charAt(x) != ' ') {
				if (str.charAt(x) == '-') {
					id = -1;
					x++;
					index = x;
					break;
				} else if (str.charAt(x) == '+') {
					index = x + 1;
					break;
				} else if (str.charAt(x) < '0' || str.charAt(x) > '9') {
					return 0;
				} else {

					index = x;
					break;
				}
			}
		}
		long nr = 0;
		for (int x = index; x < str.length(); x++) {
			char c = str.charAt(x);
			if (c >= '0' && c <= '9') {
				int i = c - 48;
				nr *= 10;
				nr += i;
			} else {
				break;
			}
		}
		nr *= id;
		if (nr ==2147483648L) {
			return 2147483647;
		}
		if (nr == -2147483648L) {
			return -2147483648;
		}
		if ((int) nr == nr) {
			return (int) nr;
		}
		return Integer.MIN_VALUE;
	}
}
