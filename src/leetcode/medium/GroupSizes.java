package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

public class GroupSizes {

	public static void main(String[] args) {
		System.out.println(groupThePeople(new int[] { 3, 3, 3, 3, 3, 1, 3 }));
	}

	public static List<List<Integer>> groupThePeople(int[] groupSizes) {
		List<List<Integer>> list = new ArrayList<>();

		List<Integer>[] tmp = new ArrayList[501];

		for (int x = 0; x < groupSizes.length; x++) {
			
			if (tmp[groupSizes[x]] == null) {
				tmp[groupSizes[x]] = new ArrayList<>();
			}
			tmp[groupSizes[x]].add(x);
			if (tmp[groupSizes[x]].size() == groupSizes[x]) {
				list.add(tmp[groupSizes[x]]);
				tmp[groupSizes[x]] = null;
			}
		}

		return list;
	}
//	public static List<List<Integer>> groupThePeople(int[] groupSizes) {
//
//		List<List<Integer>> list = new ArrayList<>();
//
//		Map<Integer, List<Integer>> map = new HashMap<>();
//
//		for (int x = 0; x < groupSizes.length; x++) {
//			Integer tmp = groupSizes[x];
//			if (map.containsKey(tmp)) {
//				map.get(tmp).add(x);
//			} else {
//				map.put(tmp, new ArrayList<>(List.of(x)));
//			}
//			var group = map.get(tmp);
//			if (group.size() == tmp) {
//				list.add(group);
//				map.remove(tmp);
//			}
//		}
//
//		return list;
//	}
}
