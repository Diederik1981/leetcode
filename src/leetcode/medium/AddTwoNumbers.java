package leetcode.medium;

import java.math.BigInteger;

import leetcode.ListNode;

/**
 * Definition for singly-linked list. public class ListNode { int val; ListNode
 * next; ListNode() {} ListNode(int val) { this.val = val; } ListNode(int val,
 * ListNode next) { this.val = val; this.next = next; } }
 */
class AddTwoNumbers {
	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {

		ListNode l3 = new ListNode();
		ListNode next = l3;
		int carry = 0;
		do {
			int tmp = l1.val + l2.val + carry;
			carry = tmp / 10;
			System.out.println(carry);
			next.next = new ListNode(tmp % 10);
			next = next.next;
			l1 = l1.next;
			l2 = l2.next;
		} while (l1 != null && l2 != null);
		ListNode rest = null;

		if (l2 != null) {
			rest = l2;
		} else {
			rest = l1;
		}
		if (rest != null) {
			do {

				int tmp = rest.val + carry;
				carry = tmp / 10;
				next.next = new ListNode(tmp % 10);
				next = next.next;
				rest = rest.next;
			} while (rest != null);
		}
		if (carry == 1) {
			next.next = new ListNode(1);
		}
		return l3.next;

	}

	public static void main(String[] args) {

		ListNode l1 = new ListNode(9);
		l1.next = new ListNode(9);
		l1.next.next = new ListNode(9);
		l1.next.next.next = new ListNode(9);

		ListNode l2 = new ListNode(9);
		l2.next = new ListNode(9);

		// System.out.println(l1);
		// System.out.println(l2);

		System.out.println(addTwoNumbers(l1, l2));

	}

	// [2,4,3]
	// [5,6,4]
}