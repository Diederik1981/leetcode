package leetcode.medium;

public class LongestSubString {
	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstring("babad"));
		
		String chars ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
		
		for (int x = 0; x < chars.length(); x++) {
			System.out.println((int)chars.charAt(x));
		}
		
	}

	public static int lengthOfLongestSubstring(String s) {
		
		boolean[] arr = new boolean[140];
		int max = 0;
		int p1 = 0;
		int counter = 0;
		for (int p2 = 0; p2 < s.length(); p2++) {
			if (arr[s.charAt(p2)]) {

				if (counter > max) {
					max = counter;
				}
				for (int x = p1; s.charAt(p2) != s.charAt(x); x++) {
					arr[s.charAt(p1)] = false;
					counter--;
					p1++;
				}
				p1++;

			} else {
				counter++;
				arr[s.charAt(p2)] = true;
			}
		}
		if (counter > max) {
			max = counter;
		}
		return max;
	}

}
