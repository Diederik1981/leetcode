package leetcode.medium;

class Cashier {

	private int discount;
	private int n;
	private int discountCounter = 0;
	private static int prices[] = new int[10001];

	public Cashier(int n, int discount, int[] products, int[] prices) {
		this.n = n;
		this.discount = discount;

		for (int x = 0; x < products.length; x++) {
			this.prices[products[x]] = prices[x];
		}

	}

	public double getBill(int[] product, int[] amount) {
		double count = 0;
		for (int x = 0; x < product.length; x++) {
			count += prices[product[x]] * amount[x];
		}
		discountCounter++;
		if (++discountCounter == n) {
			discountCounter = 0;
			count -= (discount * count) / 100;
		}

		return count;
	}

	public static void main(String[] args) {
		Cashier cashier = new Cashier(3, 50, new int[] { 1, 2, 3, 4, 5, 6, 7 },
				new int[] { 100, 200, 300, 400, 300, 200, 100 });
		System.out.println(cashier.getBill(new int[] { 1, 2 }, new int[] { 1, 2 })); // return 500.0, bill = 1 * 100 + 2
																						// * 200 = 500.
		System.out.println(cashier.getBill(new int[] { 3, 7 }, new int[] { 10, 10 })); // return 4000.0
		System.out.println(cashier.getBill(new int[] { 1, 2, 3, 4, 5, 6, 7 }, new int[] { 1, 1, 1, 1, 1, 1, 1 })); // return
																													// 800.0,
																													// The
																													// bill
		// was 1600.0 but as
		// this is the third
		// customer, he has a
		// discount of 50% which
		// means his bill is
		// only 1600 - 1600 *
		// (50 / 100) = 800.
//    	cashier.getBill([4],[10]);                           // return 4000.0
//    	cashier.getBill([7,3],[10,10]);                      // return 4000.0
//    	cashier.getBill([7,5,3,1,6,4,2],[10,10,10,9,9,9,7]); // return 7350.0, Bill was 14700.0 but as the system counted three more customers, he will have a 50% discount and the bill becomes 7350.0
//    	cashier.getBill([2,3,5],[5,3,2]);                    // return 2500.0

		/**
		 * Your Cashier object will be instantiated and called as such: Cashier obj =
		 * new Cashier(n, discount, products, prices); double param_1 =
		 * obj.getBill(product,amount);
		 */

	}
}