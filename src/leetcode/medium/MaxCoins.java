package leetcode.medium;

import java.util.Arrays;

public class MaxCoins {

	public static void main(String[] args) {
		System.out.println(maxCoins(new int[] { 9, 8, 7, 6, 5, 1, 2, 3, 4 }));
		System.out.println(maxCoins(new int[] { 2, 4, 1, 2, 7, 8 }));
		System.out.println(maxCoins(new int[] { 7, 5, 7, 7, 8, 8, 5, 10, 7 }));
	}

	public static int maxCoins(int[] piles) {
		Arrays.sort(piles);
		int sum = 0;
		int i = piles.length - 2;
		int j = 0;
		while (j++ < piles.length / 3) {
			sum += piles[i];
			i -= 2;
		}
		return sum;
	}
}
