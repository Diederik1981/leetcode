package leetcode.medium;

public class MinimumStepsToAnagram {

	public static void main(String[] args) {
		System.out.println(minSteps("bab", "aba"));
	}

	public static int minSteps(String s, String t) {
		int[] hits = new int['a' + 26];

		for (int x = 0; x < s.length(); x++) {
			hits[s.charAt(x)]++;
			hits[t.charAt(x)]--;
		}

		int diff = 0;
		for (int x = 'a'; x <= 'z'; x++) {
			if (hits[x] > 0) {
				diff += hits[x];
			}
		}
		return diff;
	}
}
