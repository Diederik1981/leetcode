package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

    public static void main(String[] args) {
        System.out.println(spiralOrder(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}}));
    }

    public static List<Integer> spiralOrder(int[][] matrix) {

        int row = 0;
        int col = -1;

        List<Integer> response = new ArrayList<>(matrix.length * matrix[0].length);

        int right = matrix[0].length;
        int down = matrix.length - 1;
        int left = right - 1;
        int up = down - 1;

        int size = matrix.length * matrix[0].length;
        while (true) {

            for (int x = 0; x < right; x++) {
                col++;
                response.add(matrix[row][col]);
            }
            right--;
            right--;
            if (response.size() >= size) {
                return response;
            }

            for (int x = 0; x < down; x++) {
                row++;
                response.add(matrix[row][col]);
            }
            down--;
            down--;
            if (response.size() >= size) {
                return response;
            }

            for (int x = 0; x < left; x++) {
                col--;
                response.add(matrix[row][col]);
            }
            left--;
            left--;
            if (response.size() >= size) {
                return response;
            }

            for (int x = 0; x < up; x++) {
                row--;
                response.add(matrix[row][col]);
            }
            up--;
            up--;
            if (response.size() >= size) {
                return response;
            }
        }


    }
}
