package leetcode.medium;

public class ContainerWithMostWater {


    public static void main(String[] args) {
        System.out.println(maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
        System.out.println(maxArea(new int[]{0, 1}));
        System.out.println(maxArea(new int[]{1, 1}));
    }


    public static int maxArea(int[] height) {


        int leftIndex = 0;
        int rightIndex = height.length - 1;

        int maxleft = height[leftIndex];
        int maxRight = height[rightIndex];
        int dist = height.length - 1;
        int max = Math.min(maxleft, maxRight) * rightIndex - leftIndex;

        while (leftIndex + 1 < rightIndex) {
            dist--;
            if (maxleft < maxRight) {
                if (height[++leftIndex] > maxleft) {
                    maxleft = height[leftIndex];
                    int tmp = Math.min(maxleft, maxRight) * dist;
                    max = Math.max(max, tmp);
                }
            } else {
                if (height[--rightIndex] > maxRight) {
                    maxRight = height[rightIndex];
                    int tmp = Math.min(maxleft, maxRight) * dist;
                    max = Math.max(max, tmp);
                }
            }
        }
        return max;
    }


    // STOLEN

//    public int maxArea(int[] height) {
//        int maxArea = 0;
//        int left = 0;
//        int right = height.length - 1;
//        while(left < right){
//            int area = 0;
//            if(height[left] > height[right]){
//                area = height[right] * (right - left);
//                right--;
//            }else{
//                area = height[left] * (right - left);
//                left++;
//            }
//            if(area > maxArea) maxArea = area;
//        }
//
//        return maxArea;
//
//    }
}
