package leetcode.biweekly;

public class HighestAltitude {


    public static void main(String[] args) {
        int i = largestAltitude(new int[]{
                -5, 1, 5, 0, -7
        });

        System.out.println(i);


        System.out.println(largestAltitude(new int[]{-4, -3, -2, -1, 4, 3, 2}));
    }

    public static int largestAltitude(int[] gain) {
        int highest = 0;

        int current = 0;

        for (int x : gain) {
            current += x;
            highest = Math.max(highest, current);
        }

        return highest;
    }
}
