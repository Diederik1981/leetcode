package leetcode.biweekly;

public class WaysToFillArray {


    public static void main(String[] args) {
        int[][] m = generateMatrix(5);

        for (int[] a : m) {
            System.out.println();
            for (int b : a) {
                System.out.printf("%02d,", b);
            }
        }
    }

    public static int[][] generateMatrix(int n) {
        int[][] matrix = new int[n][n];
        int line, column, cursor = 1;
        for(int zoom = 0; zoom < n/2; zoom++) {
            line = (column = zoom);
            for (; column < (n-1) - zoom; column++) matrix[line][column] = cursor++;
            for (; line < n - zoom - 1; line++) matrix[line][column] = cursor++;
            for (; column > zoom; column--) matrix[line][column] = cursor++;
            for (; line > zoom; line--) matrix[line][column] = cursor++;
        }
        if ((n & 1) == 1) matrix[n/2][n/2] = cursor;
        return matrix;
    }
}
